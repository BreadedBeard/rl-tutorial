extends Node2D

const Map = preload("res://Floor/Map.gd")
const math = preload("res://math.gd")
#const FGHT = preload("res://Entity/component/fighter.gd")
#const AI = preload("res://Entity/component/ai.gd")
#const INV = preload("res://Entity/component/inventory.gd")
#const STR = preload("res://Entity/component/stairs.gd")
const ITEM_FUNC = preload("res://Entity/items/item_functions.gd")

signal player_placement(pos)

var floor_map = [] # A 2D array of Tiles
var Entities = []
var mouse_pos = Vector2(0,0)
var upstairs = null
var downstairs = null

func _draw():
	# Draw the Fog-of-War tiles
	for x in range(GLOBAL.MAP_WIDTH):
		for y in range(GLOBAL.MAP_HEIGHT):
			if not floor_map[x][y].visible:
				draw_rect(Rect2(Vector2(x, y) *GLOBAL.GRID_SIZE, Vector2(1, 1) * GLOBAL.GRID_SIZE),
				Color(0,0,0, 0.7), true)
	draw_rect(Rect2(mouse_pos* GLOBAL.GRID_SIZE, Vector2(1, 1) * GLOBAL.GRID_SIZE),
			  Color(1,1,1), false)
			
func generate_floor_grids(max_rooms, room_min_size, room_max_size, floor_level):
	for x in range(GLOBAL.MAP_WIDTH):
		floor_map.append([])
		for y in range(GLOBAL.MAP_HEIGHT):
			floor_map[x].append(Map.Tile.new(true))
	
	var rooms = [] # Keep track of what rooms are generated
	var player_coord
	var center_of_last_room
	
	for r in range(max_rooms):
		var w = math.randint(room_min_size, room_max_size)
		var h = math.randint(room_min_size, room_max_size)
		var x = math.randint(0, GLOBAL.MAP_WIDTH - w - 1)
		var y = math.randint(0, GLOBAL.MAP_HEIGHT - h - 1)
		
		var new_room = Map.gridRect.new(x, y, w, h)
		var intersected = false
		for other_room in rooms:
			if new_room.intersect(other_room):
				intersected = true
				break
		
		if not intersected:
			create_room(new_room)
			var new_room_center = new_room.center()
			center_of_last_room = new_room_center
			if len(rooms) == 0:
				player_coord = new_room_center
				emit_signal("player_placement", player_coord)
			else:	
				var prev_room_coord = rooms[-1].center()
				if math.randint(0, 1):
					create_h_tunnel(prev_room_coord.x, new_room_center.x, prev_room_coord.y)
					create_v_tunnel(prev_room_coord.y, new_room_center.y, new_room_center.x)
				else:
					create_v_tunnel(prev_room_coord.y, new_room_center.y, prev_room_coord.x)
					create_h_tunnel(prev_room_coord.x, new_room_center.x, new_room_center.y)
			place_entities(new_room, floor_level, player_coord)
			rooms.append(new_room)
			
	# Create the stairs
	var stair_scene = load("res://Entity/Others/Stairs.tscn")
	downstairs = stair_scene.instance()
	var stairs_comp = STR.stairs.new(floor_level+1)
	downstairs.set_stairs_comp(stairs_comp)
	downstairs.set_grid_pos(center_of_last_room)
	insert_entity(downstairs)
		
	if floor_level>0:
		upstairs = stair_scene.instance()
		stairs_comp = STR.stairs.new(floor_level-1)
		upstairs.set_stairs_comp(stairs_comp)
		upstairs.set_grid_pos(player_coord)
		upstairs.get_node("Sprite").set_flip_h(true) 
		insert_entity(upstairs)
	
	calc_FOV_shadowcast(player_coord)

func create_room(room):
	for x in range(room.x1+1, room.x2):
		for y in range(room.y1+1, room.y2):
			floor_map[x][y].empty_tile()
	
	# Randomly add walls to a room
	if math.randint(0,1):
		for i in range(math.randint(1,5)):
			var wall_x = math.randint(room.x1+1, room.x2-1)
			var wall_y = math.randint(room.y1+1, room.y2-1)
			floor_map[wall_x][wall_y].fill_tile()
			
func create_h_tunnel(x1, x2, y):
	for x in range(min(x1, x2), max(x1, x2)+1):
		floor_map[x][y].empty_tile()
		
func create_v_tunnel(y1, y2, x):
	for y in range(min(y1, y2), max(y1, y2)+1):
		floor_map[x][y].empty_tile()
		
func place_entities(room, dungeon_level, player_coord):
	var max_monsters_per_room = from_dungeon_level([[2, 0], [3, 3], [5, 6]], dungeon_level)
	var max_items_per_room = from_dungeon_level([[1, 0], [2, 3]], dungeon_level)
	var number_of_monsters = math.randint(0, max_monsters_per_room)
	var number_of_items = math.randint(0, max_items_per_room)
	
	var monster_chances = {'orc':80, 'troll':from_dungeon_level([[15, 2], [30, 4], [60, 7]], dungeon_level)}
	var item_chances = {'healing_potion':35, 
	'sword': from_dungeon_level([[5, 3]], dungeon_level),
	'shield': from_dungeon_level([[15, 7]], dungeon_level),
	'lightning_scroll':from_dungeon_level([[25, 3]], dungeon_level),
	'fireball_scroll': from_dungeon_level([[25, 5]], dungeon_level), 
	'confusion_scroll': from_dungeon_level([[10, 1]], dungeon_level)}
	
	for i in range(number_of_monsters):
		var x = math.randint(room.x1 +1, room.x2 -1)
		var y = math.randint(room.y1 +1, room.y2 -1)

		if not check_occupied_at(Vector2(x, y)) and Vector2(x, y) != player_coord:
			var monster_choice = math.random_choice_from_dict(monster_chances)
			var monster
			var fighter_comp
			var ai_comp
			if monster_choice == 'orc':
				var orc = load("res://Entity/monster/Orc.tscn")
				monster = orc.instance()
				fighter_comp = FGHT.Fighter.new(20, 0, 4, 35)
				ai_comp = AI.BasicMonster.new()
			else:
				var troll = load("res://Entity/monster/Troll.tscn")
				monster = troll.instance()
				fighter_comp = FGHT.Fighter.new(30, 2, 8, 100)
				ai_comp = AI.BasicMonster.new()
			monster.set_fighter_comp(fighter_comp)
			monster.set_ai_comp(ai_comp)
			monster.set_grid_pos(Vector2(x, y))
			insert_entity(monster)
			
	for i in range(number_of_items):
		var x = math.randint(room.x1 +1, room.x2 -1)
		var y = math.randint(room.y1 +1, room.y2 -1)
		
		if not check_occupied_at(Vector2(x, y)):
			var item_choice = math.random_choice_from_dict(item_chances)
			var item
			if item_choice == 'healing_potion':
				var potion = load("res://Entity/items/Potion.tscn")
				item = potion.instance()
				item.set_item_comp(INV.Item.new("heal", {'amount':40}))
			elif item_choice == 'sword':
				var sword_scene = load("res://Entity/equipments/Sword.tscn")
				var sword = sword_scene.instance()
				sword.set_equippable_comp(EQP.Equippable.new(EQP.EQUIP_SLOTS.MAIN_HAND, 3, 0, 0))
				sword.set_label_name("Ordinary Sword")
			elif item_choice == 'shield':
				var shield_scene = load("res://Entity/equipments/Shield.tscn")
				var shield = shield_scene.instance()
				shield.set_equippable_comp(EQP.Equippable.new(EQP.EQUIP_SLOTS.OFF_HAND, 0, 1, 0))
				shield.set_label_name("Ordinary Shield")
			elif item_choice == 'fireball_scroll':
				var scroll = load("res://Entity/items/Scroll.tscn")
				item = scroll.instance()
				item.set_label_name("Scroll of Fire")
				item.set_item_comp(INV.Item.new("cast_fireball", {'damage':25, 'radius':3}, 
				true, 'Left-click a target tile for the fireball, or right-click to cancel.'))
			elif item_choice == 'confusion_scroll':
				var scroll = load("res://Entity/items/Scroll.tscn")
				item = scroll.instance()
				item.set_label_name("Scroll of Confuse Monster")
				item.set_item_comp(INV.Item.new("cast_confuse", {}, 
				true, 'Left-click a target enemy to confuse it, or right-click to cancel.'))
			else:
				var scroll = load("res://Entity/items/Scroll.tscn")
				item = scroll.instance()
				item.set_label_name("Scroll of Lightning")
				item.set_item_comp(INV.Item.new("cast_lightning", {'damage':40, 'max_range':5}))
				
			item.set_grid_pos(Vector2(x, y))
			insert_entity(item)
			
func from_dungeon_level(table, dungeon_level):
	table.invert()
	for values in table:
		if dungeon_level >= values[1]:
			return values[0]
			
	return 0

func remove_entity(ent):
	Entities.erase(ent)
	get_node(".").remove_child(ent)
	
func insert_entity(ent):
	Entities.append(ent)
	get_node(".").add_child(ent)
	
func check_blocking_wall(destination):
	return floor_map[destination.x][destination.y].blocked

func check_blocking_entity(destination):
	for entity in Entities:
		if entity.blocks and entity.current_grid_pos.x == destination.x and entity.current_grid_pos.y == destination.y:
			return entity
	return null

func check_item(pos):
	for entity in Entities:
		if entity.item and entity.current_grid_pos.x == pos.x and entity.current_grid_pos.y == pos.y:
			return entity
	return null
	
func check_occupied_at(pos):
	for entity in Entities:
		if entity.current_grid_pos.x == pos.x and \
		entity.current_grid_pos.y == pos.y:
			return true
	return false

func move_enemies(player):
	var messages = []
	for entity in Entities:
		if entity.ai:
			var enemy_turn_results = entity.ai.take_turn(player, self)
			for res in enemy_turn_results:
				
				if res.has('message'):
					messages.append(res['message'])
					#print(res['message'])
					
				if res.has('dead'):
					if res['dead'] == player:
						messages.append(res['dead'].kill_player())
						return [true, messages]
					else:
						messages.append(res['dead'].kill_monster())
	return [false, messages]

#----------------------------- FOV CALCULATIONs---------------------------------
func calc_FOV_shadowcast(pos):
	"""FOV using recursive shadowcasting algorithm is from:
		http://www.roguebasin.com/index.php?title=FOV_using_recursive_shadowcasting
	"""
	clear_FOV()
	floor_map[pos.x][pos.y].visible = true
	for region in range(8):
		_cast_light(pos, 1, 1.0, 0.0, GLOBAL.MAX_FOV_RADIUS, GLOBAL.MULT[0][region], GLOBAL.MULT[1][region],
		 GLOBAL.MULT[2][region], GLOBAL.MULT[3][region])
	for ent in Entities:
		if ent.fighter:
			ent.set_visible(floor_map[ent.current_grid_pos.x][ent.current_grid_pos.y].visible)
		else:
			ent.set_visible(floor_map[ent.current_grid_pos.x][ent.current_grid_pos.y].explored)
			
	update()
			

func _cast_light(start_point, row, start, end, radius, xx, xy, yx, yy):
	if start < end:
		return
	var r_squared = radius * radius
	
	for j in range(row, radius+1):
		var dir = Vector2(-j-1, -j)
		var blocked = false
		var new_start
		
		while dir.x <= 0:
			dir.x += 1
			var POS = start_point + Vector2(dir.x*xx + dir.y*xy, dir.x*yx + dir.y*yy)
			
			if not math.within_bounds(POS.x, 0, GLOBAL.MAP_WIDTH-1) or not math.within_bounds(POS.y, 0, GLOBAL.MAP_HEIGHT-1) :
				break
			
			var l_slope = (dir.x-0.5)/(dir.y+0.5)
			var r_slope = (dir.x+0.5)/(dir.y-0.5)
			if start < r_slope:
				continue
			elif end > l_slope:
				break
			else:
				if dir.length_squared() < r_squared:
					floor_map[POS.x][POS.y].visible = true
					if not floor_map[POS.x][POS.y].explored:
						floor_map[POS.x][POS.y].explored = true
						if floor_map[POS.x][POS.y].blocked:
							$FloorTiles.set_cell(POS.x, POS.y, 0)
						else:
							$FloorTiles.set_cell(POS.x, POS.y, 1)
				if blocked:
					if floor_map[POS.x][POS.y].blocked:
						new_start = r_slope
						continue
					else:
						blocked = false
						start = new_start
				else:
					if floor_map[POS.x][POS.y].blocked and j<radius:
						blocked = true
						_cast_light(start_point, j+1, start, l_slope, radius, xx, xy, yx, yy)
						new_start = r_slope
		if blocked:
			break

func clear_FOV():
	for x in range(GLOBAL.MAP_WIDTH):
		for y in range(GLOBAL.MAP_HEIGHT):
			floor_map[x][y].visible = false
			
func check_in_FOV(pos):
	return floor_map[pos.x][pos.y].visible
	
#------------------------------- Save & Load---------------------------------
func save_floor_data(player):
	var floor_dict = {}
	
	var tiles_infos = []
	for x in range(GLOBAL.MAP_WIDTH):
		tiles_infos.append([])
		for y in range(GLOBAL.MAP_HEIGHT):
			tiles_infos[x].append(floor_map[x][y].tile_info())
	floor_dict['tiles'] = tiles_infos
	
	floor_dict['player_index'] = Entities.find(player)
	var entities_info = []
	for ent in Entities:
		entities_info.append(ent.entity_dict())
	floor_dict['entities'] = entities_info
	return floor_dict
	
func load_floor_data(floor_data, player, floor_level):
	var tiles_datas = floor_data['tiles']
	for x in range(GLOBAL.MAP_WIDTH):
		floor_map.append([])
		for y in range(GLOBAL.MAP_HEIGHT):
			var tile_data = tiles_datas[x][y]
			floor_map[x].append(Map.Tile.new(tile_data['blocked'],tile_data['block_sight'],
											 tile_data['visible'],tile_data['explored']))
			if floor_map[x][y].explored:
				if floor_map[x][y].blocked: 
					$FloorTiles.set_cell(x, y, 0)
				else:
					$FloorTiles.set_cell(x, y, 1)
	var ents_data = floor_data['entities']
	var player_index = floor_data['player_index']
	
	for i in range(len(ents_data)):
		if i == player_index:
			load_player_data(player,ents_data[i])
			insert_entity(player)
		else:
			var ent = load_entity_from_data(ents_data[i])
			if ent.stairs:
				if ent.stairs.next_floor>floor_level:
					downstairs = ent
				else:		
					upstairs = ent
					upstairs.get_node("Sprite").set_flip_h(true) 
			insert_entity(ent)
	
	calc_FOV_shadowcast(player.current_grid_pos)
			
func load_entity_from_data(ent_data):
	var ent_scene = load(ent_data['scene'])
	var ent = ent_scene.instance()
	if ent_data['fighter']:
		var fighter_comp = FGHT.Fighter.new(10, 0, 3)
		fighter_comp.set_fighter_dict(ent_data['fighter'])
		ent.set_fighter_comp(fighter_comp)
	if ent_data['ai_info']:
		var ai_comp = AI.generate_ai_component(ent_data['ai_info'])
		ent.set_ai_comp(ai_comp)			
	if ent_data['item']:
		var item_data = ent_data['item']
		ent.set_item_comp(INV.Item.new(item_data['function_name'], item_data['function_args'],
										item_data['targeting'],item_data['targeting_msg']))
	if ent_data['inventory']:
		var inv_data = ent_data['inventory']
		ent.set_inventory_comp(INV.Inventory.new(inv_data['capacity']))
		for item_data in inv_data['items']:
			var item = load_entity_from_data(item_data)
			ent.inventory.items.append(item)
	if ent_data['stairs']:
		var stairs_comp = STR.stairs.new(ent_data['stairs'])
		ent.set_stairs_comp(stairs_comp)
	if ent_data['equippable']:
		var equippable_comp = EQP.Equippable.new(0)
		equippable_comp.set_equippable_info(ent_data['equippable'])
		ent.set_equippable_comp(equippable_comp)
	
	ent.set_sprite(ent_data['anim'])
	ent.blocks = ent_data['blocks']
	ent.set_label_name(ent_data['name'])
	var pos = ent_data['position']
	ent.set_grid_pos(Vector2(pos[0], pos[1]))
	
	return ent
	
func load_player_data(player, data):
	var fighter_comp = FGHT.Fighter.new(10, 0, 3)
	fighter_comp.set_fighter_dict(data['fighter'])
	player.set_fighter_comp(fighter_comp)
	var inv_data = data['inventory']
	player.set_inventory_comp(INV.Inventory.new(inv_data['capacity']))
	for item_data in inv_data['items']:
		var item = load_entity_from_data(item_data)
		player.inventory.items.append(item)
	player.level.set_level(data['level'])
	
	for equip_index in data['equipments']:
		if equip_index>=0:
			player.equipment.toggle_equip(player.inventory.items[equip_index])
			
	player.set_label_name(data['name'])
	var pos = data['position']
	player.set_grid_pos(Vector2(pos[0], pos[1]))
	