class Tile:
	var blocked
	var block_sight
	var visible
	var explored
	
	func _init(blocked, block_sight=null, visible=false, explored = false):
		self.blocked = blocked
		if block_sight == null:
			self.block_sight = blocked
		else:
			self.block_sight = block_sight
		self.visible = visible
		self.explored = explored
		
	func empty_tile():
		blocked = false
		block_sight = false
		
	func fill_tile():
		blocked = true
		block_sight = true

	func tile_info():
		var tile_dict = {}
		tile_dict['blocked'] = blocked
		tile_dict['block_sight'] = block_sight
		tile_dict['visible'] = visible
		tile_dict['explored'] = explored
		
		return tile_dict
			
class gridRect:
	var x1
	var y1
	var x2
	var y2
	
	func _init(x, y, w, h):
		x1 = x
		y1 = y
		x2 = x + w
		y2 = y + h
	
	func center():
		return(Vector2(int((x1+x2)/2), int((y1+y2)/2)))
		
	func intersect(other):
		return(self.x1 <= other.x2 and self.x2 >= other.x1 and
			   self.y1 <= other.y2 and self.y2 >= other.y1)

