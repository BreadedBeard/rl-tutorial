extends Camera2D
"""Track a given target, if specified
"""
var track_target = null

func _physics_process(delta):
	if track_target:
		position = track_target.position
