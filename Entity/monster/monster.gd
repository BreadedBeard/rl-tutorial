extends "res://Entity/Entity.gd"

func kill_monster():
	var death_msg = "%s is dead!" % [label_name]
	$Sprite.set_animation("Dead")
	blocks = false
	fighter = null
	ai = null
	label_name = "Remains of" + label_name
	set_z_index(-1)
	
	return(death_msg)
	
func get_sprite():
	return $Sprite.get_animation()
	
func set_sprite(label):
	$Sprite.set_animation(label)

