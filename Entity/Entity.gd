extends Node2D
var Grid_astar = preload("res://Entity/component/astar_grid.gd")
var astar_node = Grid_astar.Astar_grid.new(self)

var current_grid_pos = Vector2(0, 0)
var blocks = false
var label_name = ""

# The components of an entity, use the set_*_comp functions to set the components
var fighter = null
var ai = null
var item = null
var inventory = null
var stairs = null
var level = null
var equipment = null
var equippable = null

# For A Star debugging
#var _point_path = null
#const BASE_LINE_WIDTH = 3.0
#const DRAW_COLOR = Color('#fff')
#var DRAW_COLOR = Color(randf(), randf(), randf())

func _ready():
	set_grid_pos(current_grid_pos)

func set_grid_pos(pos):
	current_grid_pos = pos
	current_grid_pos.x = clamp(pos.x, 0, GLOBAL.MAP_WIDTH-1)
	current_grid_pos.y = clamp(pos.y, 0, GLOBAL.MAP_HEIGHT-1)

	position = current_grid_pos * GLOBAL.GRID_SIZE
	
func set_fighter_comp(fighter_comp):
	fighter = fighter_comp
	fighter.comp_owner = self
	
func set_ai_comp(ai_comp):
	ai = ai_comp
	ai.comp_owner = self
	
func set_inventory_comp(inv_comp):
	inventory = inv_comp
	inventory.comp_owner = self
	
func set_item_comp(item_comp):
	item = item_comp
	item.comp_owner = self
	
func set_stairs_comp(stairs_comp):
	stairs = stairs_comp
	stairs_comp.comp_owner = self
	
func set_level_comp(level_comp):
	level = level_comp
	level_comp.comp_owner = self

func set_equipment_comp(equipment_comp):
	equipment = equipment_comp
	equipment_comp.comp_owner = self
	
func set_equippable_comp(equippable_comp):
	equippable = equippable_comp
	equippable_comp.comp_owner = self
	if not item:
		#var INV = load("res://Entity/component/inventory.gd")
		set_item_comp(INV.Item.new())
		
func move_towards(target_coord, game_map):
	"""Basic move towards a point function
	"""
	var dd = target_coord - current_grid_pos
	var d_move = Vector2(sign(dd.x), sign(dd.y))
	print(d_move)
	if not (game_map.check_blocking_wall(current_grid_pos+d_move) \
	or game_map.check_blocking_entity(current_grid_pos+d_move)):
			set_grid_pos(current_grid_pos+d_move)
			
func move_astar(target_coord, game_map):
	"""Move using the A star algorithm
	"""
	astar_node.astar_connect_walkable_cells_diagonal(astar_node.astar_add_walkable_cells(game_map))
	var path_array = astar_node.calculate_path(current_grid_pos, target_coord)
	if len(path_array)>1:
		set_grid_pos(Vector2(path_array[1].x, path_array[1].y))
	else:
		move_towards(target_coord, game_map)
		
	#_update_draw_path(path_array) # Uncomment for A star Debugging
	astar_node.clear_grid()
	
func set_label_name(name):
	label_name = name
	
func entity_dict():
	"""Return a dictionary of information about the entity
	"""
	var data_dict = {}
	data_dict['scene'] = get_filename()
	data_dict["position"] = [current_grid_pos.x, current_grid_pos.y]
	data_dict['blocks'] = blocks
	data_dict['name'] = label_name
	data_dict['anim'] = get_sprite() 
	if fighter:
		data_dict['fighter'] = fighter.fighter_dict()
	else:
		data_dict['fighter'] = fighter
	if ai:
		data_dict['ai_info'] = ai.ai_info()
	else:
		data_dict['ai_info'] = ai
	if item:
		data_dict['item']=item.item_info()
	else:
		data_dict['item']=item
	if inventory:
		data_dict['inventory']=inventory.inventory_info()
	else:
		data_dict['inventory']=inventory
		
	if stairs:
		data_dict['stairs']=stairs.stairs_info()
	else:
		data_dict['stairs']=stairs
		
	if level:
		data_dict['level']=level.level_info()
		
	if equippable:
		data_dict['equippable'] = equippable.equippable_info()
	else:
		data_dict['equippable'] = equippable
		
	if equipment:
		var equips = equipment.get_equips()
		
		var equips_index = []
		for equip in equips:
			if equip:
				equips_index.append(inventory.items.find(equip))
			else:
				equips_index.append(-1)
		data_dict['equipments'] = equips_index
	
	return data_dict

func get_sprite():
	return null
	
func set_sprite(label):
	pass

"""	
func_update_draw_path(path_array):
	_point_path = path_array
	update()

func _draw():
	if not _point_path:
		return
	var point_start = _point_path[0]
	var point_end = _point_path[len(_point_path) - 1]
	var half_grid_size = Vector2(1, 1) * GLOBAL.GRID_SIZE/2
	#set_cell(point_start.x, point_start.y, 1)
	#et_cell(point_end.x, point_end.y, 2)

	var last_point = grid_to_world(Vector2(point_start.x, point_start.y)) + half_grid_size
	for index in range(1, len(_point_path)):
		var current_point = grid_to_world(Vector2(_point_path[index].x, _point_path[index].y)) + half_grid_size
		draw_line(last_point, current_point, DRAW_COLOR, BASE_LINE_WIDTH, true)
		draw_circle(current_point, BASE_LINE_WIDTH * 2.0, DRAW_COLOR)
		last_point = current_point
		
func grid_to_world(pos):
	return (Vector2(pos.x-current_grid_pos.x, pos.y-current_grid_pos.y)*GLOBAL.GRID_SIZE)
"""