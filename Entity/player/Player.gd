extends "res://Entity/Entity.gd"

#var FGHT = preload("res://Entity/component/fighter.gd")
#var INV = preload("res://Entity/component/inventory.gd")
#var LVL = preload("res://Entity/component/levels.gd")
#var EQP = preload("res://Entity/component/equips.gd")

signal movement_pressed(next_step)
signal pickup_pressed(current_step)
signal go_down_level()
signal go_up_level()

func _ready():
	label_name = "Player"
	set_fighter_comp(FGHT.Fighter.new(100, 1, 2))
	set_inventory_comp(INV.Inventory.new(26))
	set_level_comp(LVL.Level.new())
	set_equipment_comp(EQP.Equipment.new())
	
func starting_out():	
	var dagger_scene = load("res://Entity/equipments/Dagger.tscn")
	var dagger = dagger_scene.instance()
	dagger.set_label_name("Dagger")
	dagger.set_equippable_comp(EQP.Equippable.new(EQP.EQUIP_SLOTS.MAIN_HAND, 2,0,0))
	inventory.add_item(dagger)
	equipment.toggle_equip(dagger)
	
func _input(event):
	if Input.is_action_just_pressed("ui_up"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(0, -1))
	elif Input.is_action_just_pressed("ui_down"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(0, 1))
	elif Input.is_action_just_pressed("ui_left"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(-1, 0))
	elif Input.is_action_just_pressed("ui_right"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(1, 0))
	elif Input.is_action_just_pressed("ui_leftup"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(-1, -1))
	elif Input.is_action_just_pressed("ui_leftdown"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(-1, 1))
	elif Input.is_action_just_pressed("ui_rightup"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(1, -1))
	elif Input.is_action_just_pressed("ui_rightdown"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(1, 1))
	elif Input.is_action_just_pressed("ui_wait"):
		emit_signal("movement_pressed", current_grid_pos+Vector2(0, 0))
	elif Input.is_action_just_pressed("ui_pickup"):
		emit_signal("pickup_pressed", current_grid_pos)
	elif Input.is_action_just_pressed("ui_godown"):
		emit_signal("go_down_level")
	elif Input.is_action_just_pressed("ui_goup"):
		emit_signal("go_up_level")
		
func kill_player():
	$Sprite.set_animation("Dead")
	return("Player is killed")
