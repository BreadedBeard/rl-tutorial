class Astar_grid:
	var astar_node = AStar.new()
	var comp_owner = null
	
	func _init(comp_owner):
		self.comp_owner = comp_owner
	
	func astar_add_walkable_cells(game_map):
		var points_array = []
		for x in range(GLOBAL.MAP_WIDTH):
			for y in range(GLOBAL.MAP_HEIGHT):
				var point = Vector2(x, y)
				if game_map.check_blocking_wall(point): 
					continue
				var block_ent = game_map.check_blocking_entity(point)
				if block_ent and block_ent != comp_owner:
					continue
					
				points_array.append(point)
				var point_index = calculate_point_index(point)
				astar_node.add_point(point_index, Vector3(point.x, point.y, 0.0))
		return points_array
		
	func astar_connect_walkable_cells_diagonal(points_array):
		for point in points_array:
			var point_index = calculate_point_index(point)
			for local_y in range(3):
				for local_x in range(3):
					var point_relative = Vector2(point.x + local_x - 1, point.y + local_y - 1)
					var point_relative_index = calculate_point_index(point_relative)
	
					if point_relative == point or is_outside_map_bounds(point_relative):
						continue
					if not astar_node.has_point(point_relative_index):
						continue
					astar_node.connect_points(point_index, point_relative_index, true)
	
	func calculate_path(source, dest):
		var start_point_index = calculate_point_index(source)
		var end_point_index = calculate_point_index(dest)
		var path = astar_node.get_point_path(start_point_index, end_point_index)
		return(path)
		
	func is_outside_map_bounds(point):
		return point.x < 0 or point.y < 0 or point.x >= GLOBAL.MAP_WIDTH or point.y >= GLOBAL.MAP_HEIGHT
	
	func calculate_point_index(point):
		return point.x + GLOBAL.MAP_WIDTH * point.y
		
	func clear_grid():
		astar_node.clear()