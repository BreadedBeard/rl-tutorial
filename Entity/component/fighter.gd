extends Node

class Fighter:
	var base_max_hp
	var max_hp
	var hp
	var base_defense
	var defense
	var base_power
	var power
	var xp
	var comp_owner
	
	func _init(hp, defense, power, xp=0):
		base_max_hp = hp
		self.hp = hp
		base_defense = defense
		base_power = power
		self.xp = xp
		update_stats()
		
	func update_stats():
		var hp_bonus = 0
		var power_bonus = 0
		var defense_bonus = 0
		if comp_owner and comp_owner.equipment:
			hp_bonus = comp_owner.equipment.max_hp_bonus()
			power_bonus = comp_owner.equipment.power_bonus()
			defense_bonus = comp_owner.equipment.defense_bonus()
			
		max_hp = base_max_hp + hp_bonus
		power = base_power + power_bonus
		defense = base_defense + defense_bonus
		
	func take_damage(amount):
		var results = []
		hp -= amount
		
		if hp <= 0:
			results.append({'dead': comp_owner, 'xp': xp})
		
		return results
		
	func heal(amount):
		hp += amount
		if hp >max_hp:
			hp = max_hp
		
	func attack(target):
		var results = []
		var damage = power - target.fighter.defense
		
		if damage > 0:
			results.append({'message':'%s attacks %s for %d hit points.'%[comp_owner.label_name, target.label_name, damage]})
			results = results + target.fighter.take_damage(damage)
		else:
			results.append({'message':'%s attacks %s but does no damage.'%[comp_owner.label_name, target.label_name]})
			
		return results
		
	func fighter_dict():
		var data_dict = {}
		data_dict['max_hp'] = base_max_hp
		data_dict['hp'] = hp
		data_dict['defense'] = base_defense
		data_dict['power'] = base_power
		data_dict['xp'] = xp
		return data_dict
		
	func set_fighter_dict(data_dict):
		base_max_hp = data_dict['max_hp']
		hp = data_dict['hp']
		base_defense = data_dict['defense']
		base_power = data_dict['power']
		xp = data_dict['xp']