extends Node

class Inventory:
	var capacity
	var items = []
	var comp_owner
	var UTIL = preload("res://utility.gd")
	
	func _init(capacity):
		self.capacity = capacity
		
	func add_item(item):
		var results = []
		
		if len(items) >= capacity:
			results.append({
                'item_added': null,
                'message': 'You cannot carry any more, your inventory is full'
            })
		else:
			results.append({
                'item_added': item,
                'message': 'You pick up the %s!'%[item.label_name]
            })
			items.append(item)
		return results
		
	func use(item_entity, args={}):
		var results = []
		
		var item_comp = item_entity.item
		
		if item_comp.use_function == null:
			var equippable_comp = item_entity.equippable
			if equippable_comp:
				results.append({'equip':item_entity})
			else:
				results.append({'message':'The %s cannot be used'%item_entity.label_name})
		else:
			if item_comp.targeting and not (args.has("target")):
				results.append({'targeting':item_entity})
			else:
				UTIL.merge_dict(args, item_comp.function_args)
				UTIL.merge_dict(args, {"user": self.comp_owner})
				var item_use_results = item_comp.use_function.call_func(args)
				for res in item_use_results:
					if res.has("consumed") and res["consumed"]:
						self.remove_item(item_entity)
						item_entity.queue_free()
				results = results + item_use_results
			
		return results
		
	func remove_item(item):
		items.erase(item)
		
	func drop_item(item):
		var results = []
		
		if comp_owner.equipment.main_hand == item or comp_owner.equipment.off_hand == item:
			comp_owner.equipment.toggle_equip(item)
		
		item.set_grid_pos(comp_owner.current_grid_pos)
		remove_item(item)
		results.append({'item_dropped': item, 'message': 'You dropped the %s'%item.label_name})
		return results
		
	func inventory_info():
		var data_dict = {}
		data_dict['capacity'] = capacity
		var items_infos = []
		for item_ent in items:
			items_infos.append(item_ent.entity_dict())
		data_dict['items'] = items_infos
		return data_dict
		
class Item:
	var comp_owner = null
	var function_name = ''
	var use_function = null
	var function_args
	var targeting
	var targeting_msg
	var ITEM_FUNC = preload("res://Entity/items/item_functions.gd")
	
	func _init(function_name=null, function_args=null, targeting=false, targeting_msg=null):
		self.function_name = function_name
		if function_name:
			self.use_function = funcref(ITEM_FUNC, function_name)
			
		self.function_args = function_args
		self.targeting = targeting
		self.targeting_msg = targeting_msg
		
	func item_info():
		var data_dict = {}
		data_dict['function_name'] = function_name
		data_dict['function_args'] = function_args
		data_dict['targeting'] = targeting
		data_dict['targeting_msg'] = targeting_msg
		return data_dict