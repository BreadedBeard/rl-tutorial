extends Node

enum EQUIP_SLOTS{
	MAIN_HAND,
	OFF_HAND
}

class Equippable:
	var slot
	var power_bonus
	var defense_bonus
	var max_hp_bonus
	var comp_owner = null
	
	func _init(slot, power_bonus=0, defense_bonus=0, max_hp_bonus=0):
		self.slot = slot
		self.power_bonus = power_bonus
		self.defense_bonus = defense_bonus
		self.max_hp_bonus = max_hp_bonus
		
	func equippable_info():
		return [slot, power_bonus, defense_bonus, max_hp_bonus]
	
	func set_equippable_info(data):
		slot = data[0]
		power_bonus = data[1]
		defense_bonus = data[2]
		max_hp_bonus = data[3]
		
class Equipment:
	var main_hand = null
	var off_hand = null
	var comp_owner = null
	
	func max_hp_bonus():
		var bonus = 0
		if main_hand and main_hand.equippable:
			bonus += main_hand.equippable.max_hp_bonus
		if off_hand and off_hand.equippable:
			bonus += off_hand.equippable.max_hp_bonus
		return bonus
		
	func power_bonus():
		var bonus = 0
		if main_hand and main_hand.equippable:
			bonus += main_hand.equippable.power_bonus
		if off_hand and off_hand.equippable:
			bonus += off_hand.equippable.power_bonus
		return bonus
		
	func defense_bonus():
		var bonus = 0
		if main_hand and main_hand.equippable:
			bonus += main_hand.equippable.defense_bonus
		if off_hand and off_hand.equippable:
			bonus += off_hand.equippable.defense_bonus
		return bonus
		
	func toggle_equip(equippable_ent):
		var results = []
		var slot = equippable_ent.equippable.slot
		
		if slot == EQUIP_SLOTS.MAIN_HAND:
			if main_hand == equippable_ent:
				main_hand = null
				results.append({'dequipped':equippable_ent})
			else:
				if main_hand:
					results.append({'dequipped':main_hand})
				main_hand = equippable_ent
				results.append({'equipped':equippable_ent})
		if slot == EQUIP_SLOTS.OFF_HAND:
			if off_hand == equippable_ent:
				off_hand = null
				results.append({'dequipped':equippable_ent})
			else:
				if off_hand:
					results.append({'dequipped':off_hand})
				off_hand = equippable_ent
				results.append({'equipped':equippable_ent})
				
		comp_owner.fighter.update_stats()
		return results
		
	func get_equips():
		return [main_hand, off_hand]
