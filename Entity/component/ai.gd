extends Node

class BasicMonster:
	var comp_owner = null
	var ai_name = "BasicMonster"
	
	func take_turn(target, game_map):
		var results = []
		if game_map.check_in_FOV(comp_owner.current_grid_pos):
			if comp_owner.current_grid_pos.distance_to(target.current_grid_pos)>=2:
				comp_owner.move_astar(target.current_grid_pos, game_map)
			elif target.fighter.hp>0:
				results = results + comp_owner.fighter.attack(target)
				
		return results
	
	func ai_info():
		return {'type': ai_name}
		
class ConfusedMonster:
	var comp_owner = null
	var previous_ai
	var number_of_turns
	var math = preload("res://math.gd")
	var ai_name = "ConfusedMonster"
	
	func _init(previous_ai, number_of_turns):
		self.previous_ai = previous_ai
		self.number_of_turns = number_of_turns
		self.comp_owner = previous_ai.comp_owner
		
	func take_turn(target, game_map):
		var results = []
		
		if number_of_turns>0:
			var random_pos = comp_owner.current_grid_pos + Vector2(math.randint(0,2)-1, math.randint(0,2)-1)
			
			if random_pos.x != comp_owner.current_grid_pos.x and random_pos.y != comp_owner.current_grid_pos.y:
				comp_owner.move_towards(random_pos, game_map)
			number_of_turns -= 1
		else:
			comp_owner.set_ai_comp(previous_ai)
			results.append({'message': 'The %s is no longer confused!'%comp_owner.label_name})
			
		return results
		
	func ai_info():
		return {'type': ai_name, 'info':[previous_ai.ai_info(), number_of_turns]}
		
static func generate_ai_component(ai_info):
	var ai_comp
	if ai_info['type'] == "BasicMonster":
		ai_comp =  BasicMonster.new()
	elif ai_info['type'] == "ConfusedMonster":
		assert len(ai_info['info']) == 2
		ai_comp = ConfusedMonster.new(generate_ai_component(ai_info['info'][0]),ai_info['info'][1])
	return ai_comp