extends Node

class Level:
	var current_level
	var current_xp
	var level_up_base
	var level_up_factor
	var comp_owner
	
	func _init(current_level=1, current_xp=0, level_up_base=200, level_up_factor=150):
		self.current_level = current_level
		self.current_xp = current_xp
		self.level_up_base = level_up_base
		self.level_up_factor = level_up_factor
		
	func experience_to_next_level():
		return level_up_base + current_level * level_up_factor
		
	func add_xp(xp):
		current_xp += xp
		
		if current_xp >= experience_to_next_level():
			var level_ups = 0
			while current_xp >= experience_to_next_level():
				current_xp -= experience_to_next_level()
				level_ups += 1
			current_level += level_ups
			return level_ups
		else:
			return 0
			
	func set_level(data):
		current_level = data[0]
		current_xp = data[1]
		level_up_base = data[2]
		level_up_factor = data[3]
		
	func level_info():
		return [current_level, current_xp, level_up_base, level_up_factor]