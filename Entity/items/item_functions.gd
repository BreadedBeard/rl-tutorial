func heal(args):
	assert args.has("user")
	assert args.has("amount")
	
	var target = args["user"]
	var amount = args["amount"]
	var results = []
	
	if target.fighter.hp == target.fighter.max_hp:
		results.append({'consumed': false, 'message': 'You are already at full health'})
	else:
		target.fighter.heal(amount)
		results.append({'consumed': true, 'message': 'Your wounds start to feel better!'})
		
	return results
	
func cast_lightning(args):
	assert args.has("user")
	assert args.has("floormap")
	assert args.has("damage")
	assert args.has("max_range")
	
	var user = args["user"]
	var entities = args["floormap"].Entities
	var dmg = args["damage"]
	var max_range = args["max_range"]
	
	var results = []
	
	var target = null
	var closest_distance = max_range + 1
	
	for ent in entities:
		if ent.fighter and ent != user and ent.is_visible():
			var dist = user.current_grid_pos.distance_to(ent.current_grid_pos)
			if dist < closest_distance:
				target = ent
				closest_distance = dist
				
	if target:
		results.append({'consumed': true, 'target': target, 
		'message': 'A lighting bolt strikes the %s with a loud thunder! The damage is %d'%[target.label_name, dmg]})
		results = results + target.fighter.take_damage(dmg)
	else:
		results.append({'consumed': false, 'target': null, 
		'message': 'No enemy is close enough to strike.'})
	
	return results
	
func cast_fireball(args):
	assert args.has("floormap")
	assert args.has("damage")
	assert args.has("radius")
	assert args.has("target")
	
	var floor_map = args["floormap"]
	var entities = floor_map.Entities
	var dmg = args["damage"]
	var radius = args["radius"]
	var target_pos = args["target"]
	
	var results = []
	if not floor_map.check_in_FOV(target_pos):
		results.append({'consumed': false, 'message': 'You cannot target a tile outside your field of view.'})
		return results
		
	results.append({'consumed': true, 'message': 'The fireball explodes, burning everything within %d tiles!'%radius})

	for entity in entities:
		if entity.fighter and entity.current_grid_pos.distance_to(target_pos) <= radius:
			results.append({'message': 'The %s gets burned for %d hit points.'%[entity.label_name, dmg]})
			results = results + entity.fighter.take_damage(dmg)

	return results
	
func cast_confuse(args):
	assert args.has("floormap")
	assert args.has("target")
	
	var floor_map = args["floormap"]
	var entities = floor_map.Entities
	var target_pos = args["target"]
	
	var results = []
	if not floor_map.check_in_FOV(target_pos):
		results.append({'consumed': false, 'message': 'You cannot target a tile outside your field of view.'})
		return results
	
	var target_found = false
	for ent in entities:
		if ent.current_grid_pos.x == target_pos.x and ent.current_grid_pos.y == target_pos.y and ent.ai:
			var AI = load("res://Entity/component/ai.gd")
			var confused_ai = AI.ConfusedMonster.new(ent.ai, 10)
			ent.ai = confused_ai
			results.append({'consumed': true, 'message': 'The eyes of the %s look vacant, as he starts to around!'%ent.label_name})
			target_found = true
			break
	if not target_found:
		results.append({'consumed': false, 'message': 'There is no targetable enemy at that location.'})
			
	return results