extends Node
"""The scene where the game takes place. Generally, player inputs processing take place here,
along with interfaces with other components
"""

# The game works as a FSM, only certain action takes place in certain game state
enum GAME_STATE{
	PLAYER_TURN,
	ENEMY_TURN,
	PLAYER_DEAD,
	TARGETING,
	LEVEL_UP
}

# Signals to be emitted at certain events, should be should self-explanatory
signal update_player_stats(player)
signal display_messages(messages)
signal update_player_inv(item)
signal item_targeting(item)
signal player_level_up(player, level_ups)

var current_floor = 0
var floors = []	# List of the generated floor nodes
var floorScene = preload("res://Floor/Floor.tscn")	# For generating floors
var game_state = GAME_STATE.PLAYER_TURN
var prev_game_state = null
var targeting_item = null

# For ease of access
onready var player = get_node("Player")
onready var view = $GameLayout/View_container/View
onready var cam = $GameLayout/View_container/View/Cam1

func _ready():
	#think more about saving seed for dungeon generation
	randomize()
	
	# Set up the cam to track the player, and set up the player's signal for processing
	cam.track_target = player
	emit_signal("update_player_stats", player)
	player.connect("movement_pressed", self, "_on_Player_movement_pressed")
	player.connect("pickup_pressed", self, "_on_Player_pickup_pressed")
	player.connect("go_down_level", self, "_on_Player_godown_pressed")
	player.connect("go_up_level", self, "_on_Player_goup_pressed")
	
	# Either load a game or generate a new game
	if GLOBAL.loading_game:
		load_game()
		GLOBAL.loading_game = false
	else:
		player.starting_out()
		generate_floor()
	
	emit_signal("update_player_inv", player)
	emit_signal("update_player_stats", player)
	
# For displaying the mouse position in the grid
func _process(delta):
	floors[current_floor].mouse_pos = get_mouse_position_in_map()
	floors[current_floor].update()
	
func _input(event):
	if event.is_action_pressed("ui_fullscreen"):
		OS.set_window_fullscreen(not OS.is_window_fullscreen())
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()		
	if Input.is_action_just_pressed("ui_save"):
		save_game()
	
	if game_state == GAME_STATE.TARGETING:
		# Handle mouse input for targetting
		var player_turn_results = []
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			player_turn_results = player.inventory.use(targeting_item, {"floormap":floors[current_floor], "target": get_mouse_position_in_map()})
		elif Input.is_mouse_button_pressed(BUTTON_RIGHT):
			player_turn_results = [{'targeting_cancelled': true}]
		
		_process_player_results(player_turn_results)
		if game_state == GAME_STATE.ENEMY_TURN:
			post_player_turn()
	else:
		# Process mouse wheel for zooming and panning
		#Should add a check for whether mouse is in the view
		if Input.is_mouse_button_pressed(BUTTON_WHEEL_UP):
			var cam_zoom = cam.get_zoom()-Vector2(0.1,0.1)
			cam.set_zoom(cam_zoom)
		elif Input.is_mouse_button_pressed(BUTTON_WHEEL_DOWN):
			var cam_zoom = cam.get_zoom()+Vector2(0.1,0.1)
			cam.set_zoom(cam_zoom)
		if Input.is_mouse_button_pressed(BUTTON_MIDDLE):
			if event is InputEventMouseMotion:
				cam.set_offset(cam.get_offset()-event.get_relative())
			
func save_game():
	var save_file = File.new()
	save_file.open("res://testsave.save", File.WRITE)
	
	var save_data = {}
	save_data['current_floor'] = current_floor
	var floors_infos = []
	for flr in floors:
		floors_infos.append(flr.save_floor_data(player))
	save_data['floors'] = floors_infos
	save_data['game_state'] = game_state
	
	var data = to_json(save_data)
	save_file.store_line(data)
	save_file.close()
	print('------Saved!-------')
	
func load_game():
	var save_file = File.new()
	if not save_file.file_exists("res://testsave.save"):
		print('------No Save File!-------')
		return # Error! We don't have a save to load.
	save_file.open("res://testsave.save", File.READ)
	
	var text = save_file.get_as_text()
	
	var save_data = parse_json(text)
	save_file.close()
	
	current_floor = save_data['current_floor']
	game_state = save_data['game_state']
	
	var floors_datas = save_data['floors']
	for i in range(len(save_data['floors'])):
		generate_floor(floors_datas[i], i==current_floor)
	
	print('------Loaded!-------')
	
func generate_floor(floor_data=null, is_current_floor=true):
	"""Generate a floor, whether data is given or not
	
	floor_data: dict
		The data of the floor, generally comes from a save file. However, special floor
		can be synthesize this way
	is_current_floor: bool
		If true, the room generated will be put into view, and place the player in it.
		Don't otherwise.
	"""
	var floor_room = floorScene.instance()
	if is_current_floor:
		# This is need to place the player once the floor is loaded
		# Alternatively, passing the player into the generation function would avoid the need of this function
		floor_room.connect("player_placement", player, "set_grid_pos", [], CONNECT_ONESHOT)
		
	if floor_data:
		floor_room.load_floor_data(floor_data, player, len(floors))
	else:
		floor_room.generate_floor_grids(GLOBAL.MAX_ROOMS, GLOBAL.ROOM_MIN_SIZE, GLOBAL.ROOM_MAX_SIZE, current_floor)
	if is_current_floor:
		#Reparent the player and the room to be display in the viewport
		view.add_child(floor_room)
		remove_child(player)
		floor_room.add_child(player)
		floor_room.Entities.append(player)
		
	floors.append(floor_room)

func post_player_turn():
	"""This function is called once the player takes their turn
	"""
	var status = floors[current_floor].move_enemies(player)
	emit_signal("update_player_stats", player)
	emit_signal("display_messages", status[1])
	if status[0]:
		game_state = GAME_STATE.PLAYER_DEAD
	else:
		floors[current_floor].calc_FOV_shadowcast(player.current_grid_pos)
		game_state = GAME_STATE.PLAYER_TURN

func _on_Player_movement_pressed( next_step ):
	if game_state == GAME_STATE.PLAYER_TURN:
		var player_turn_results = []
		if not floors[current_floor].check_blocking_wall(next_step):
			var blocking_target = floors[current_floor].check_blocking_entity(next_step)
			if blocking_target:
				player_turn_results = player_turn_results + player.fighter.attack(blocking_target)
			else:
				player.set_grid_pos(next_step)
				
			game_state = GAME_STATE.ENEMY_TURN
		
			_process_player_results(player_turn_results)
			
func _on_Player_pickup_pressed(current_pos):
	var player_turn_results = []
	if game_state == GAME_STATE.PLAYER_TURN:
		var item = floors[current_floor].check_item(current_pos)
		if item:
			player_turn_results = player_turn_results + player.inventory.add_item(item)
			floors[current_floor].remove_entity(item)
			emit_signal("update_player_inv", player)
			game_state = GAME_STATE.ENEMY_TURN
		else:
			player_turn_results.append({'message':'There is nothing here to pick up.'})
			
		_process_player_results(player_turn_results)

func _on_Player_godown_pressed():
	var ent = floors[current_floor].downstairs
	
	if ent:
		if ent.current_grid_pos == player.current_grid_pos:
			_goto_level(ent.stairs.next_floor)
		else:
			_process_player_results([{'message' : "There's no downstairs here" }])
		
func _on_Player_goup_pressed():
	var ent = floors[current_floor].upstairs
	if ent:
		if ent.current_grid_pos == player.current_grid_pos:
			_goto_level(ent.stairs.next_floor)
		else:
			_process_player_results([{'message' : "There's no upstairs here" }])

func _goto_level(level):
	var floor_room
	# Generate new level if it goes over the number of generated floors
	if level>len(floors)-1:
		print('Generating next level')
		floor_room = floorScene.instance()
		floor_room.connect("player_placement", player, "set_grid_pos", [], CONNECT_ONESHOT)
		floor_room.generate_floor_grids(GLOBAL.MAX_ROOMS, GLOBAL.ROOM_MIN_SIZE, GLOBAL.ROOM_MAX_SIZE, level)
		floors.append(floor_room)
	else:
		floor_room = floors[level]
		if level<current_floor:
			player.set_grid_pos(floor_room.downstairs.current_grid_pos)		
		else:
			player.set_grid_pos(floor_room.upstairs.current_grid_pos)
	
	#Reparent the player and the room to be display in the viewport
	floors[current_floor].remove_child(player)
	floors[current_floor].Entities.erase(player)
	view.remove_child(floors[current_floor])
	
	view.add_child(floor_room)
	floor_room.add_child(player)
	floor_room.Entities.append(player)
	current_floor = level
	
	floors[current_floor].calc_FOV_shadowcast(player.current_grid_pos)
	# Heal back half of the damage taken, might remove this???
	player.fighter.heal(int((player.fighter.max_hp-player.fighter.hp)/ 2))
	emit_signal("update_player_stats", player)
	game_state = GAME_STATE.ENEMY_TURN
	_process_player_results([{'message' : 'You take a moment to rest, and recover your strength.' }])
	
func _on_HUD_using_item( item ):
	var player_turn_results = []
	if game_state == GAME_STATE.PLAYER_TURN:
		player_turn_results = player_turn_results + player.inventory.use(item, {"floormap":floors[current_floor]})
	_process_player_results(player_turn_results)

func _on_HUD_dropitem_pressed( item ):
	var player_turn_results = []
	if game_state == GAME_STATE.PLAYER_TURN:
		player_turn_results = player_turn_results + player.inventory.drop_item(item)
	
	_process_player_results(player_turn_results)
	
func _process_player_results(results):
	cam.set_offset(Vector2(0,0))
	var messages = []
	for res in results:
		if res.has('dead'):
			if res['dead'] == player:
				messages.append(player.kill_player())
				game_state = GAME_STATE.PLAYER_DEAD
			else:
				messages.append(res['dead'].kill_monster())
		if res.has('consumed') and res["consumed"]:
			emit_signal("update_player_inv", player)
			game_state = GAME_STATE.ENEMY_TURN
		if res.has('item_dropped') and res["item_dropped"]:
			floors[current_floor].insert_entity(res["item_dropped"])
			emit_signal("update_player_inv", player)
			game_state = GAME_STATE.ENEMY_TURN
		if res.has('targeting'):
			prev_game_state = game_state
			game_state = GAME_STATE.TARGETING
			targeting_item = res['targeting']
			#emit_signal("item_targeting", res['targeting']) #Might need this later
			messages.append(res['targeting'].item.targeting_msg)
		if res.has('targeting_cancelled') and res['targeting_cancelled']:
			game_state = prev_game_state
			targeting_item = null
			messages.append("Targeting cancelled")
		if res.has('xp'):
			var level_ups = player.level.add_xp(res['xp'])
			messages.append("You gain %d experience points."%res["xp"])
			if level_ups>0:
				messages.append("Your battle skills grow stronger! You reached level %d !"%player.level.current_level)
				prev_game_state = game_state
				game_state = GAME_STATE.LEVEL_UP
				emit_signal("player_level_up", player, level_ups)
		if res.has('equip'):
			var equip_results = player.equipment.toggle_equip(res['equip'])
			for equip_result in equip_results:
				if equip_result.has('equipped'):
					messages.append('You equipped the %s'%equip_result['equipped'].label_name)
				if equip_result.has('dequipped'):
					messages.append('You dequipped the %s'%equip_result['dequipped'].label_name)
			game_state = GAME_STATE.ENEMY_TURN
			emit_signal("update_player_inv", player)
			emit_signal("update_player_stats", player)
		if res.has('message'):
			messages.append(res['message'])
		
	if messages:
		emit_signal("display_messages", messages)
	
	if game_state == GAME_STATE.ENEMY_TURN:
		post_player_turn()
		
func get_mouse_position_in_map():
	var pos = ((cam.get_camera_position() + cam.get_offset()) / cam.get_zoom() - view.get_size()/2 + view.get_mouse_position()) /(GLOBAL.GRID_SIZE/cam.get_zoom().x)
	return Vector2(clamp(int(pos.x), 0, GLOBAL.MAP_WIDTH-1), clamp(int(pos.y) ,0 ,GLOBAL.MAP_HEIGHT-1))

func _on_LevelUp_menu_stats_selected( options ):
	if options == 0:
		player.fighter.base_max_hp += 20
		player.fighter.hp += 20
		emit_signal("display_messages", ["Health increased"])
	elif options == 1:
		player.fighter.base_power += 1
		emit_signal("display_messages", ["Power increased"])
	elif options == 2:
		player.fighter.base_defense += 1
		emit_signal("display_messages", ["Defense increased"])
	player.fighter.update_stats()
	emit_signal("update_player_stats", player)

func _on_LevelUp_menu_finish_levelup():
	game_state = prev_game_state
	if game_state == GAME_STATE.ENEMY_TURN:
		post_player_turn()
