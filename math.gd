static func randint(lower, upper):
	return randi() % (upper - lower + 1) + lower
	
static func within_bounds(val, lower, upper, inclusive = true):
	if inclusive:
		return val<=upper and val>=lower
	else:
		return val<upper and val>lower
		
static func random_choice_index(chances):
	var sum_chances = 0
	for val in chances:
		sum_chances += val
	var random_chance = randint(1, sum_chances)
	
	var running_sum = 0
	var choice = 0
	for w in chances:
		running_sum += w
		
		if random_chance <= running_sum:
			return choice
		choice += 1
		
static func random_choice_from_dict(choice_dict):
	var choices = choice_dict.keys()
	var chances = choice_dict.values()
	
	return choices[random_choice_index(chances)]
