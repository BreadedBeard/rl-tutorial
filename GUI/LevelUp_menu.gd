extends Node2D

# class member variables go here, for example:
# var a = 2
var levels_up = 0
signal stats_selected(options)
signal finish_levelup()

func _ready():
	hide()

func show_menu(player, levels_up):
	self.levels_up = levels_up
	$Panel/Popup/Vbox/Con_btn.set_text('Constitution (+20 HP, from %d)'%player.fighter.max_hp)
	$Panel/Popup/Vbox/Str_btn.set_text('Strength (+1 attack, from %d)'%player.fighter.power)
	$Panel/Popup/Vbox/Agi_btn.set_text('Agility (+1 defense, from %d)'%player.fighter.defense)
	show()

func _on_Con_btn_pressed():
	emit_signal("stats_selected",0)
	levels_up -= 1
	_check_remaining_levelups()
	
func _on_Str_btn_pressed():
	emit_signal("stats_selected",1)
	levels_up -= 1
	_check_remaining_levelups()

func _on_Agi_btn_pressed():
	emit_signal("stats_selected",2)
	levels_up -= 1
	_check_remaining_levelups()
	
func _check_remaining_levelups():	
	if levels_up == 0:
		hide()
		emit_signal("finish_levelup")
