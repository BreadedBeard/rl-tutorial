extends CanvasLayer

onready var text_log = $GUI_panel/GUI_container/console_log

signal using_item(item)
signal dropitem_pressed(item)

func _on_Root_update_player_stats(player):
	$GUI_panel/GUI_container/Stats_container/HP_label.set_text("HP: %d/%d" %[player.fighter.hp, player.fighter.max_hp])
	$GUI_panel/GUI_container/Stats_container/Lvl_label.set_text("Lvl: %d" %player.level.current_level)
	$GUI_panel/GUI_container/Stats_container/XP_label.set_text("XP: %d/%d" %[player.level.current_xp, player.level.experience_to_next_level()])
	$GUI_panel/GUI_container/Stats_container/Pwr_label.set_text("Str: %d" %player.fighter.power)
	$GUI_panel/GUI_container/Stats_container/Def_label.set_text("Def: %d" %player.fighter.defense)

func _on_Root_display_messages( messages ):
	for msg in messages:
		text_log.add_text(msg + '\n')

func _on_Root_update_player_inv( player ):
	$Inventory_list.clear()
	for i in range(len(player.inventory.items)):
		var item = player.inventory.items[i]
		if item == player.equipment.main_hand:
			$Inventory_list.add_item(item.label_name + '(Main)')		
		elif item == player.equipment.off_hand:
			$Inventory_list.add_item(item.label_name + '(Off)')
		else:
			$Inventory_list.add_item(item.label_name)
			
		$Inventory_list.set_item_metadata(i, item)

func _on_Inventory_list_item_activated( index ):
	var item = $Inventory_list.get_item_metadata(index)
	emit_signal("using_item", item)
	
func _input(event):
	if Input.is_action_just_pressed("ui_drop"):
		var item_index = $Inventory_list.get_selected_items()
		if len(item_index):
			var item = $Inventory_list.get_item_metadata(item_index[0])
			emit_signal("dropitem_pressed", item)
