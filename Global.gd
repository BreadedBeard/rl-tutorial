extends Node

const GRID_SIZE = 32
const MAP_WIDTH = 50
const MAP_HEIGHT = 50

const ROOM_MAX_SIZE = 10
const ROOM_MIN_SIZE = 6
const MAX_ROOMS = 30

const FOV_RADIUS = 10

const MAX_MONSTERS_PER_ROOM = 3
const MAX_ITEMS_PER_ROOM = 3

# For FOV calculation
const MULT=[
[1,  0,  0, -1, -1,  0,  0,  1],
[0,  1, -1,  0,  0, -1,  1,  0],
[0,  1,  1,  0,  0, -1, -1,  0],
[1,  0,  0,  1, -1,  0,  0, -1],
]
const MAX_FOV_RADIUS = 7

var loading_game = false