extends Node2D

func _on_newgame_btn_pressed():
	get_tree().change_scene("res://game_scene.tscn")
	
func _on_loadgame_btn_pressed():
	GLOBAL.loading_game = true
	get_tree().change_scene("res://game_scene.tscn")
